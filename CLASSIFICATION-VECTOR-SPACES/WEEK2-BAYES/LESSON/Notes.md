
$$ P(X|Y) = P(Y|X) * \frac{P(X)}{P(Y)}$$
